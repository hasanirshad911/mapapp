import {Component} from "react";
import React from 'react';
import { Switch, Route, Link } from "react-router-dom";
import Home from "./Home"
import Map from "./Map"

export default class Main extends Component {
    render() {
        return (
            <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/activa' component={Map} />
                <Route exact path='/pic' component={Map} />
                <Route exact path='/saludable' component={Map} />
            </Switch>
        );
    }
}