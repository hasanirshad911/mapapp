import {Component} from "react";
import React from 'react';
import { Switch, Route, Link } from "react-router-dom";

export default class Header extends Component {
    render() {

        return (
            <header>
                <nav>
                    <ul>
                        <li><Link to='/'>Home</Link></li>
                        <li><Link to='/activa'>Active</Link></li>
                        <li><Link to='/pic'>Pic Málaga</Link></li>
                        <li><Link to='/saludable'>Málaga Saludable</Link></li>
                    </ul>
                </nav>
            </header>
        );
    }
}
