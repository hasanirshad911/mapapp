import {Component} from "react";
import React from 'react';
import {fromJS} from "immutable";
import ReactMapGL, { NavigationControl } from 'react-map-gl';

const MAPBOX_API_TOKEN = 'pk.eyJ1IjoicGVwaXRvLWdyaWxsbyIsImEiOiJjajhhdjFjN3MwZ2Y2MnFwaWlkNmtoY2Y0In0.HJNKwaFRS8_ikTesrLtVsg';
const layer = fromJS({
    sources: {
        points: {
            type: 'geojson',
            data: {
                type: 'FeatureCollection',
                features: [
                    {type: 'Feature', geometry: {type: 'Point', coordinates: [-122.45, 37.78]}}
                ]
            }
        }
    },
    layers: [
        {
            id: 'my-layer',
            type: 'circle',
            source: 'points',
            paint: {
                'circle-color': '#f00',
                'circle-radius': '4'
            }
        }
    ]
});
export default class Map extends Component {

    state = {
        viewport: {
            latitude: 36.7213,
            longitude: -4.4214,
            zoom: 12,
        },
        mapStyle: 'mapbox://styles/mapbox/streets-v9',
    };

    _resize = () => {
        this.setState({
            viewport: {
                ...this.state.viewport,
                width: this.props.width || window.innerWidth,
                height: this.props.height || window.innerHeight
            }
        });
    };

    _updateViewport = (viewport) => {
        this.setState({ viewport });
    }

    componentDidMount() {
        window.addEventListener('resize', this._resize);
        this._resize();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._resize);
    }

    render() {
        return (

            <ReactMapGL
                {...this.state.viewport}
                onViewportChange={this._updateViewport}
                MAPBOX_API_TOKEN
                mapboxApiAccessToken={MAPBOX_API_TOKEN}
                mapStyle={this.state.mapStyle}
            >
                <NavigationControl onViewportChange={this._updateViewport} />
            </ ReactMapGL>
        );
    }
}
