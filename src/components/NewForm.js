import * as firebase from "firebase";
import React from 'react';
export default class NewForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }
    TodayDate = new Date().getDate().toString();
    TodayMonth =new Date().getMonth().toString();
    TodayYear =new Date().getFullYear().toString();

    state = {
        namee: '',
        date:this.TodayDate+"/"+this.TodayMonth+"/"+this.TodayYear,
        img:'',
        long:'',
        lat:''
    };

    handleChange = e => {
        if (e.target.files[0]) {
            const image = e.target.files[0];
            this.setState(() => ({image}));
        }
    }

    atsubmission=()=> {
        // alert('A name was submitted: ' + this.state.namee);
        const rootref = firebase.database().ref('ff_form/'+ this.state.namee);

        const {image} = this.state;
        const uploadTask = this.props.storage.ref(`images/${image.name}`).put(image);
        uploadTask.on('state_changed',
            (snapshot) => {
                // progrss function ....
                const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                this.setState({progress});
            },
            (error) => {
                // error function ....
                console.log(error);
            },
            () => {
                // complete function ....
                this.props.storage.ref('images').child(image.name).getDownloadURL().then(url => {

                    this.setState({url});
                    rootref.set({
                        f_Name:this.state.namee,
                        t_Date:this.state.date,
                        img:this.state.url,
                        longitude:this.state.long,
                        latitude:this.state.lat
                    });
                })
            });
    }
    render() {
        return (
            <form>
                <h1>{this.state.progress}</h1>
                <label>
                    Name:
                </label>
                <input
                    placeholder="Name"
                    value={this.state.namee}
                    onChange={e=>this.setState({namee:e.target.value})}
                />
                <label>
                    Image:
                </label>
                <input type="file" onChange={this.handleChange}/>
                <label>
                </label>
                Date:
                <input
                    // placeholder="Date"
                    value={this.state.date}
                    readOnly={true}
                    // onChange={e=>this.setState({date:e.target.value})}
                />
                <label>
                    Longitude:

                </label>
                <input
                    type="number"
                    step="0.01"
                    placeholder="Longitude"
                    value={this.state.long}
                    onChange={e=>this.setState({long:e.target.value})}
                />
                <label>
                    Latitude:
                </label>
                <input
                    type="number"
                    step="0.01"
                    placeholder="Latitude"
                    value={this.state.lat}
                    onChange={e=>this.setState({lat:e.target.value})}
                />
                <input value="Submit" type="button" onClick={this.atsubmission}/>
            </form>
        );
    }
}
