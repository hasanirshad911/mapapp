import React, { Component } from 'react';
import 'normalize.css/normalize.css';
import './styles/styles.scss';

import * as firebase from 'firebase';
import Header from "./components/Header";
import Main from "./components/Main"
import Footer from "./components/Footer"
import NewForm from "./components/NewForm";
const config = {
    apiKey: "AIzaSyAia7cQyQB19-ZMAt5YynLbAObK8fs7GiM",
    authDomain: "malaga-maps.firebaseapp.com",
    databaseURL: "https://malaga-maps.firebaseio.com",
    projectId: "malaga-maps",
    storageBucket: "malaga-maps.appspot.com",
    messagingSenderId: "504075334223"
};

firebase.initializeApp(config);
const storage = firebase.storage();
 // storage={storage}

class App extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Main/>
                <NewForm storage={storage}/>
                <Footer/>
            </div>
        );
    }
}
export default App;